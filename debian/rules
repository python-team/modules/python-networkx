#!/usr/bin/make -f

LIB2 := $(shell python -c "from distutils.command.build import build ; from distutils.core import Distribution ; b = build(Distribution()) ; b.finalize_options() ; print b.build_platlib")
PYTHON2 := $(shell pyversions -r)

%:
	dh $@ --with python2

override_dh_auto_clean:
	dh_auto_clean

	# Add here commands to clean up after the build process.
	rm -fr build networkx/version.py test.* doc/source/templates/gallery.html fontList.cache

	# Make sure that there's no .pyc left
	find . -name '*.pyc' -exec rm {} ';'

override_dh_auto_test:
ifeq ($(filter nocheck,$(DEB_BUILD_OPTIONS)),)
	#set -e ; \
	# running tests
	-for py in $(PYTHON2) ; do\
		PYTHONPATH=$(LIB2) $$py setup.py nosetests -v ; \
	done
endif

override_dh_install:
	# Install w/o compiling *.pyc files
	# Install egg-info directories (--single-... option)
	python setup.py install --no-compile --root=$(CURDIR)/debian/python-networkx \
		--single-version-externally-managed --install-layout=deb

	# Fix executable bits:
	chmod +x debian/python-networkx/usr/lib/`pyversions -d`/*-packages/networkx/tests/test.py
	find debian/python-networkx -name '*.bz2' -exec chmod a-x {} ';'

	dh_install



DEBVERS         ?= $(shell dpkg-parsechangelog | sed -n -e 's/^Version: //p')
VERSION         ?= $(shell echo '$(DEBVERS)' | sed -e 's/^[[:digit:]]*://' -e 's/[-].*//')
NODFSG_VERSION  ?= $(shell echo '$(VERSION)' | sed -e 's/\+dfsg[[:digit:]]*//')
DEBFLAVOR       ?= $(shell dpkg-parsechangelog | grep -E ^Distribution: | cut -d" " -f2)
DEBPKGNAME      ?= $(shell dpkg-parsechangelog | grep -E ^Source: | cut -d" " -f2)
UPSTREAM_GIT    ?= git://github.com/networkx/networkx.git


override_dh_compress:
	dh_compress -Xexamples/ -X.js -X.pdf -Xobjects.inv

override_dh_installchangelogs:
	dh_installchangelogs doc/news.rst
